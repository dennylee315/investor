

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;

import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.citi.trading.Investor;
import com.citi.trading.Market;
import com.citi.trading.OrderPlacer;

import static org.hamcrest.Matchers.closeTo;


public class InvestorTest {
	
	private Investor investor;
	
	@Before 
	public void setup() {
		Map portfolio = new HashMap<String, Integer>();
		portfolio.put("MKT", 100000);
		investor = new Investor(portfolio, 10000);
	}
	
	@After
	public void tearDown() {
		investor = null;
	}
	
	@Test
	public void testBuy() {
		
		OrderPlacer market = new MockMarket();
		investor.setMarket(market);
		assertThat(investor.getCash(), closeTo(10000, 0.001));
		investor.buy("MRK", 1, 1000);
		assertThat(investor.getCash(), closeTo(9000, 0.001));
	}
	
	@Test
	public void testSell() {
		OrderPlacer market = new MockMarket();
		investor.setMarket(market);
		investor.buy("MRK", 2, 1000);
		investor.sell("MRK", 1, 1000);
		assertThat(investor.getCash(), closeTo(9000, 0.001));
	}
}
