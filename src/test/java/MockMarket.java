
import java.util.function.Consumer;


import javax.jms.Message;

import com.citi.trading.Market;
import com.citi.trading.OrderPlacer;
import com.citi.trading.Trade;

public class MockMarket implements OrderPlacer {	
	

	
	public void placeOrder(Trade order, Consumer<Trade> callback) {
		callback.accept(order);
	}

}

